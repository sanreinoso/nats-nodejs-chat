// ---------------------------------------------------------------------
// mainMiembre.js
// require
const Logica = require("./logicaProxy.js")
const readline = require("readline");

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
async function main() {

  if (process.argv[2] == undefined) {
    console.log("Uso correcto mainMiembro.js  <tu_nickname>");
    process.exit(0)
  }

  let name = process.argv[2]; 
  console.log(`--------------- MIEMBRO DEL CHAT ${name.toUpperCase()} ----------------`);

  let logica = new Logica();
  let conexion = await logica.conectar()

  //Pedimos unirse al chat
  //let confirm = await logica.addMember(name);

  logica.waitMessage();

  try {
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });

    rl.on("line", (input) => {
      logica.sendMessage(name, input);
    })

    process.on("SIGINT", async function () {
      rl.close();
      logica.cerrar()
    })
  } catch (err) {
    console.log(err);
    logica.cerrar();
  }
  //await logica.cerrar();

}

main();

