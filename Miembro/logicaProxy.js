const { connect, StringCodec, Empty } = require("nats")
const { send } = require("process")
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------


const codificador = StringCodec()
const SERVIDOR_NATS_URL = { servers: "localhost:4222" }
const ADD_MEMBER = "add"
const MESSAGE_RECV = "message.publish"
const MESSAGE_SEND = "message.send"


module.exports =
  class ProxyLogica {

    constructor() { }

    async conectar() {
      this.conexionNats = await connect(SERVIDOR_NATS_URL)
      return true;
    }

    async addMember(name) {
      let rta = await this.conexionNats.request(ADD_MEMBER, codificador.encode(name), {timeout: 3000})
        .then((m) => {
          console.log(`got response: ${codificador.decode(m.data)}`);
          return codificador.decode(m.data);
        })
        .catch((err) => {
          console.log(`problem with request: ${err.message}`,  err);
        });
     return rta;
    }

    async sendMessage(name, message) {
      await this.conexionNats.request(MESSAGE_SEND, this.success(name, message))
        .then((m) => {
          console.log("-->");
        })
        .catch((err) => {
          console.error("Fue un error", err)
        });
    }


    async waitMessage() {
      const response = await this.conexionNats.subscribe(MESSAGE_RECV, {max: 10})
      for await (const message of response) {
        try {
          var msg = JSON.parse(codificador.decode(message.data));
          console.log(`${msg.name}: ${msg.message}`);
        }
        catch (err) {
          console.log(" Upss! algo salio mal...", err)
        }
      }
    }

    success(name, msg) {
      let message = JSON.stringify({ "name": name, "message": msg })
      return codificador.encode(message);
    }

    async cerrar() {
      await this.conexionNats.close()
    }

  }
