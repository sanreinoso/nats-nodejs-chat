// IMPORTS --------------------------------
const Logica = require( "./Logica.js" )

const { connect, StringCodec } = require( "nats" )

// ---------------------------------------------------------------------

const codificador = StringCodec()
const SERVIDOR_NATS_URL =  { servers: "localhost:4222" }
const MI_NOMBRE = ( process.argv[2] ? process.argv[2] : "chatServer" )
const ADD = "add";
const MESSAGE_RECV = "message.send";
const MESSAGE_SEND = "message.publish"

// ---------------------------------------------------------------------
// Conectamos a NATS y nos suscribimos a los dos Subjects que tenemos
// add ----> Agrega el nombre del usuario a un arreglo de nombres
// message.send --> Se suscribe a los mensjaes enviados por cada uno de los participantes
// message.publish ---> Difunde los mensajes a todos los suscritos
// ---------------------------------------------------------------------
async function main() {

	console.log( "----------------- CHAT SERVER START -----------------" )
	const logica = new Logica();

	const conexion = await connect( SERVIDOR_NATS_URL )

	// capturo ctrl-c para cerrar
	process.on('SIGINT', async function() {
		console.log (" sigint capturada ! ")
		await conexion.close()
		const err = await conexion.closed()
		if ( ! err ) {
			console.log( "desconexion ok" )
		} else {
			console.log( err )
		}
	})

	/*const resp = conexion.subscribe( ADD )
	for await( const message of resp ) {
		try{
			console.log( MI_NOMBRE + "Server he recibido una petición ")
			var name = codificador.decode(message.data);
			logica.addMember(name);
	
			message.respond( success("Agregado al chat!!"))
			
			//IMPORTANTE!!
			//Me desuscribo de este subject para poder suscribirme al otro
			resp.unsubscribe();

		}
		catch(err){
			console.log( " Upss! algo salio mal...", err)
		}
	}*/

	
// ----------------------------------------------------------------------
	const resp2 = conexion.subscribe( MESSAGE_RECV );
	for await( const message of resp2 ) {
		try{
			//Me llega el REQ del miembro del chat (mensaje)
			var msg =  codificador.decode(message.data);
			var confirm = JSON.parse(msg);
			console.log(`${confirm.name} escribio algo..`);
			message.respond( success("OK"));

			//Hago una publicación de mensaje que me envia el miembro x.
			const pub = conexion.publish(MESSAGE_SEND, codificador.encode(msg))
			
		}
		catch(err){
			console.log( " Upss! algo salio mal...", err)
		}


	//console.log(msg);	
	}

}


function success( msg ){
	let message = JSON.stringify({ status: "OK", message: msg })
	return codificador.encode(message)
}

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
main()
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
