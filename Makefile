
help:
	@echo "----------------------------------------------"
	@echo " lee Makefile "
	@echo 
	@echo "----------------------------------------------"

# -------------------------------------------------------------------
#
# utilidades sobre el contenedor nats
#
# -------------------------------------------------------------------
pullnats:
	docker pull nats
	docker images

#
# A nats server exposes multiple ports:
#  4222 for clients
#  6222 routing for clusters
#  8222 for requesting information for reports
# Fuente: hub.doocker.com -> nats container
#
runnats:
	docker run --name natssrv --rm  -p 4222:4222 nats || true

stopnats:
	docker stop natssrv
	docker ps -a

# -------------------------------------------------------------------
#
# utilidades
#
# -------------------------------------------------------------------
ls:
	docker images

ps:
	docker ps -a

clean:
	docker stop natssrv
